/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.robotproject;

/**
 *
 * @author domem
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = ' ';
    
    public Robot(int x, int y, int bx, int by, int N){
        this.x = x;
        this.y = y;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }
    public String toString(){
        return "Robot: ("+ this.x + ", "+ this.y + ") "+"Bomb point:"+"("+Math.abs(x-bx)+", "+ Math.abs(y-by)+")";
    }
    public boolean walk(char direction){
        if (direction == 'N'){
            if (!inMap(x,y-1)){
               printUnableToMove();
               return false; 
            }
            y = y-1;
        }
        else if (direction == 'S'){
            if (!inMap(x,y+1)){
                printUnableToMove();
               return false; 
            }
            y = y+1;
        }
        else if (direction == 'W'){
            if (!inMap(x-1,y)){
                printUnableToMove();
               return false; 
            }
            x = x-1;
        }
        else if (direction == 'E'){
            if (!inMap(x+1,y)){
                printUnableToMove();
               return false; 
            }
            x = x+1;
        }
        lastDirection = direction;
        isBomb();
        return true;  
    }
    public boolean walk(char direction, int step){
        for (int i = 0; i<step; i++){
            Robot.this.walk(direction);
        }
        lastDirection = direction;
        return true;
    }
    public boolean walk(){
        Robot.this.walk(lastDirection);
        return true;
    }
    public boolean walk(int step){
        Robot.this.walk(lastDirection, step);
        return true;
    }
    public boolean isBomb(){
        if (x == bx && y == by)
        {
            System.out.println("Bomb found!!");
            return true;
        }
        return false;
    }
    public boolean inMap (int x, int y){
        if (x >= N || x<0 || y >= N || y<0 ){
            return false;
        }
        return true;
    }
    public void printUnableToMove(){
        System.out.println("I can't move!!");
    }
}

