/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.robotproject;

/**
 *
 * @author domem
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0,0,10,10,100);
        robot.walk('S');
        System.out.println(robot);
        robot.walk('W');
        System.out.println(robot);
        robot.walk('E', 5);
        System.out.println(robot);
        robot.walk();
        System.out.println(robot);
        robot.walk(4);
        System.out.println(robot);
        robot.walk('S', 9);
        System.out.println(robot);      
    }
}
